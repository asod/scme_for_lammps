!-----------------------------------------------------------------------
!                  New potential based on multipole moments.
!     Each molecule is represented as a multipole expansion up to
!     hexadecapole moment.
!-----------------------------------------------------------------------
!     An arrays with positions enters as argument ra().
!     It is assumed that the array of positions ra() has a length equal
!     to 9 times the number of molecules and that its structure is the
!     following:
!           ra(l+6*(i-1)) stores the l-th coordinate of the first
!                         hydrogen in the i-th molecule
!           ra(l+3+6*(i-1)) stores the l-th coordinate of the second
!                         hydrogen in the i-th molecule
!           ra(l+3*(i-1+nHydrogens)) stores the l-th coordinate of the
!                         oxygen in the i-th molecule. (nHydrogens is
!                         the total number of hydrogen atoms)
!     The routine will return an array fa() with the forces on each atom.
!     The array fa() has the same structure as ra().
!     The total potential energy of the configuration is also calculated
!     and returned in 'uTot'.
!
!     This routine was written by Enrique Batista.
!-----------------------------------------------------------------------

!23456789012345678901234567890123456789012345678901234567890123456789012
!        10        20        30        40        50        60        70

program scme

        use data_types
        use max_parameters
        use multipole_parameters, only: d0, q0, o0, h0
        use polariz_parameters, only: dd0, dq0, hp0, qq0
        use molecProperties, only: calcCentersOfMass,&
                findPpalAxes, rotatePoles, rotatePolariz, setUnpolPoles, addFields, addDfields
        use calc_lower_order, only: calcEdip_quad
        use calc_higher_order, only: calcEhigh
        use calc_derivs, only: calcDv
        use inducePoles, only: induceDipole, induceQpole
        use forceCM_mod, only: forceCM
        use torqueCM_mod, only: torqueCM
        use atomicForces_mod, only: atomicForces
        use calcEnergy_mod, only: calcEnergy
        use coreInt_mod, only: coreInt
        use dispersion_mod, only: dispersion


        ! nAtms: number of atoms, integer
        ! raOri: atom coordinates
        ! fa: forces
        ! uTot: total energy

        implicit none

        real(dp) rMax, rMax2

        real(dp) pi, raOri(maxCoo), fa(maxCoo)
        real(dp) fCM(3,maxCoo/3), fsf(3,maxCoo/3), tau(3,maxCoo/3)
        real(dp) uD, uQ, uH, uPol, uDisp, uRho, uCore, uES

        !     atomic position, centers of mass, principal axes.
        real(dp) ra(maxCoo), rCM(3,maxCoo/3), x(3,3,maxCoo/3)

        !     Electric fields
        real(dp) eD(3,maxCoo/3), eQ(3,maxCoo/3), eH(3,maxCoo/3)
        real(dp) eT(3,maxCoo/3)
        !     Derivatives of E
        real(dp) dEddr(3,3,maxCoo/3), dEqdr(3,3,maxCoo/3)
        real(dp) dEhdr(3,3,maxCoo/3), dEtdr(3,3,maxCoo/3)

        !     High order derivatives of the potential
        real(dp) d1v(3,maxCoo/3), d2v(3,3,maxCoo/3), d3v(3,3,3,maxCoo/3)
        real(dp) d4v(3,3,3,3,maxCoo/3), d5v(3,3,3,3,3,maxCoo/3)

        real(dp) uTot,  u, convFactor
        integer nM, nO, nH, nAtms(2), i, ii, j, k, l 

        !     Multipoles
        real(dp) dpole(3,maxCoo/3), qpole(3,3,maxCoo/3)
        real(dp) opole(3,3,3,maxCoo/3), hpole(3,3,3,3,maxCoo/3)

        !     Unpolarized multipoles
        real(dp) dpole0(3,maxCoo/3), qpole0(3,3,maxCoo/3)

        !     Polarizabilities
        real(dp) dd(3,3,maxCoo/3), dq(3,3,3,maxCoo/3), hp(3,3,3,maxCoo/3)
        real(dp) qq(3,3,3,3,maxCoo/3)

        logical*1 addCore, converged

        save

        ! aod: simply hardcoding in a water trimer to test using only using the
        ! core scme potential. Should give same energy and forces as the wrapped code
        nAtms(1) = 6
        nAtms(2) = 3

!        raOri(1) =   -8.6265253272999995     
!        raOri(2) =   -5.6728194940999996     
!        raOri(3) =   -1.9500565904000000     
!        raOri(4) =   -7.1126246727000000     
!        raOri(5) =   -5.6728194940999996     
!        raOri(6) =   -1.9500565904000000     
!        raOri(7) =   -6.3780753273000004     
!        raOri(8) =   -1.7783898547000001     
!        raOri(9) =   -1.9500565912000001     
!        raOri(10) =  -4.8641746726999999     
!        raOri(11) =  -1.7783898547000001     
!        raOri(12) =  -1.9500565912000001     
!        raOri(13) =  -7.8695750000000002     
!        raOri(14) =  -5.1925728520999996     
!        raOri(15) =  -2.2856474199999997     
!        raOri(16) =  -5.6211250000000001     
!        raOri(17) =  -1.2981432121999998     
!        raOri(18) =  -2.2856474199999997    

        raOri(1) =    -0.45090479999999999     
        raOri(2) =    -0.28502780000000000     
        raOri(3) =     0.79478037999999995     
        raOri(4) =     0.84617425000000002     
        raOri(5) =    -0.44638923999999996     
        raOri(6) =     -3.0945560000000000E-002
        raOri(7) =      3.1571296399999995     
        raOri(8) =    -0.85423557000000006     
        raOri(9) =      1.0608568300000001     
        raOri(10) =     2.7241535099999994     
        raOri(11) =    0.33142845999999998     
        raOri(12) =    0.22501734000000001     
        raOri(13) =     1.1190166900000000     
        raOri(14) =     1.5614771900000000     
        raOri(15) =    0.18941036999999999     
        raOri(16) =     1.9651939700000001     
        raOri(17) =     2.5603071900000001     
        raOri(18) =   -0.57099597999999985     
        raOri(19) =      0.0000000000000000     
        raOri(20) =      0.0000000000000000     
        raOri(21) =      0.0000000000000000     
        raOri(22) =      2.7811086499999997     
        raOri(23) =    -0.62397647000000001     
        raOri(24) =     0.21125645000000001     
        raOri(25) =      1.9197764399999999     
        raOri(26) =      2.0815694900000001     
        raOri(27) =     0.25663878000000001     


       pi = 3.14159265358979312d0

       nO = nAtms(2)          ! Number of hydrogens
       nH = nAtms(1)          ! Number of oxygens
       nM = nO                ! Number of molecules


        rMax = 11.d0
        rMax2 = rMax*rMax
        !     Convert from Debye^2/angstrom^3 to eV
        convFactor = 14.39975841d0 / 4.803206799d0**2

        addCore = .TRUE.

        uTot = 0.0d0

        ra = raOri  ! old PBC relic

        call calcCentersOfMass(ra, nM, rCM)

        call findPpalAxes(ra, nM, x)    ! defines rotation matrix(?) x

        call rotatePoles(d0, q0, o0, h0, dpole0, qpole0, opole, hpole, nM, x) !defines dpole0... using x
        call setUnpolPoles(dpole, qpole, dpole0, qpole0, nM)
        call rotatePolariz(dd0, dq0, qq0, hp0, dd, dq, qq, hp, nM, x)

        call calcEhigh(rCM, opole, hpole, nM, uH, eH, dEhdr, rMax2)

        !     Here's where the induction loop begins
        converged = .false.

        do while (.not. converged)
                !         ii = ii + 1
                call calcEdip_quad(rCM, dpole, qpole, nM, uD, uQ, eD, dEddr, rMax2)

                call addFields(eH, eD, eT, nM)
                call addDfields(dEhdr, dEddr, dEtdr, nM)

                !     Induce dipoles and quadrupoles
                converged = .true.
                call induceDipole(dPole, dpole0, eT, dEtdr, dd, dq, hp, nM, converged)
                call induceQpole(qPole, qpole0, eT, dEtdr, dq, qq, nM, converged)

        end do                    !End of the induction loop

        !     With the polarized multipoles, calculate the derivarives of the
        !     electrostatic potential, up to 5th order.

        call calcDv(rCM, dpole, qpole, opole, hpole, nM, d1v, d2v, d3v, d4v, d5v, rMax2, fsf)

        !     Compute the force on the center of mass
        call forceCM(dpole, qpole, opole, hpole, d2v, d3v, d4v, d5v, nM, fsf, fCM)

        !     Compute the torque on the molecule
        call torqueCM(dpole, qpole, opole, hpole, d1v, d2v, d3v, d4v, nM, tau)

        !     Find 3 forces, one at oxygen and one at each hydrogen such that
        !     the total force and total torque agree with the ones calculated
        !     for the multipoles.

        call atomicForces(fCM, tau, ra, rCM, nM, fa) ! fa calculated here

        !     Calculate the energy of interaction between the multipole moments
        !     and the electric field of the other molecules.
        call calcEnergy(dpole0, qpole0, opole, hpole, d1v, d2v, d3v, d4v, nM, uTot)

        !PRB - Don't use this one ---------------------------------------------
        !      call calcEnergy(dpole, qpole, opole, hpole, d1v, d2v, d3v, d4v,
        !    $      nM, uTot)
        !PRB - ----------------------------------------------------------------

        !      uPol = 0.d0

        !      call calcEnergyI(dpole, dpole0, qpole, qpole0, opole,
        !     $     hpole, d1v, d2v, d3v, d4v, nM, uPol)
        !      print *, uTot*convFactor, uPol*convFactor, (uPol+uTot)*Convfactor
        !      stop

        !      print '(2f15.7,$)', uTot*convFactor, uPol*convFactor

        !      uTot = uTot - uPol

        !PRB - ----------------------------------------------------------------

        do i = 1, 3*(nH+nO)
                fa(i) = convFactor * fa(i)
        end do
        uTot = uTot * convFactor

        uES = uTot

        call dispersion(ra, fa, uDisp, nM)
        uTot = uTot + uDisp

        if (addCore) then
                111     call coreInt(ra, fa, uCore, nM)
                uTot = uTot + uCore
        end if

        print '(4f16.10)', uTot, uES, uDisp, uCore
        !$$$      stop


        return

!        end subroutine scme_calculate

end program scme

