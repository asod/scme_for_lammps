module max_parameters

        implicit none

        integer, parameter :: maxatoms = 150, maxcoo = 3*maxatoms

end module max_parameters
