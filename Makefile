# **********************************************************************************************************************************
# settings

# disable the built-in (implicit) rules to avoid trying to compile X.o from X.mod (Modula-2 program)
.SUFFIXES:

objdir = obj
moddir = mod
srcdir = src/

vpath %.f90 $(srcdir)

FC = gfortran
FFLAGS = -O3 -I$(moddir) -J$(moddir)

objects = $(addprefix $(objdir)/, \
       scme.o mdutil.o multipole_parameters.o\
       calc_higher_order.o calc_lower_order.o inducePoles.o calc_derivs.o data_types.o \
       forceCM_mod.o torqueCM_mod.o atomicForces_mod.o molforce.o tang_toennies.o \
       calcEnergy_mod.o molecProperties.o dispersion_mod.o coreInt_mod.o rho.o max_parameters.o\
       parameters.o polariz_parameters.o)


all: scme.exc

# **********************************************************************************************************************************

# linking
scme.exc: $(objects)
	$(FC) $(FFLAGS) -o $@ $(objects)
	@echo 'Success!'

# compiling

$(objdir)/%.o: %.f90
	$(FC) $(FFLAGS) -c -o $@ $<


# **********************************************************************************************************************************
# module dependencies

$(objdir)/parameters.o: $(objdir)/data_types.o

$(objdir)/tang_toennies.o: $(objdir)/parameters.o

$(objdir)/multipole_parameters.o: $(objdir)/data_types.o

$(objdir)/scme.o: $(addprefix $(objdir)/, max_parameters.o multipole_parameters.o polariz_parameters.o \
	molecProperties.o calc_higher_order.o calc_lower_order.o calc_derivs.o inducePoles.o forceCM_mod.o \
	torqueCM_mod.o atomicForces_mod.o calcEnergy_mod.o coreInt_mod.o dispersion_mod.o data_types.o)
# read_pol.o

$(objdir)/molecProperties.o: $(objdir)/tang_toennies.o

$(objdir)/calc_higher_order.o: $(objdir)/molecProperties.o

$(objdir)/calc_lower_order.o: $(objdir)/molecProperties.o

$(objdir)/calc_derivs.o: $(objdir)/molecProperties.o

$(objdir)/coreInt_mod.o: $(objdir)/rho.o

$(objdir)/atomicForces_mod.o: $(objdir)/max_parameters.o $(objdir)/molforce.o

$(objdir)/molforce.o: $(objdir)/mdutil.o


# **********************************************************************************************************************************
# cleanup

.PHONY: clean
clean:
	rm $(objdir)/*.o $(moddir)/*.mod scme.exc

